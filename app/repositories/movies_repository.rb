require "#{File.dirname(__FILE__)}/../../models/movie"
require "#{File.dirname(__FILE__)}/../../models/movie_not_found"

class OmdbRepository
  def initialize(api_key, api_url)
    @api_key = api_key
    @api_url = api_url
  end

  def find_movie(title)
    response = Faraday.get(@api_url, { t: title, apikey: @api_key })
    response_json = JSON.parse(response.body)

    raise MovieNotFound if response_json['Response'] == 'False'

    awards_overview = response_json['Awards']
    Movie.new(title, awards_overview)
  end
end
