class Movie
  attr_accessor :awards_overview

  OVERVIEW_NO_AWARDS = 'No known awards'.freeze

  def initialize(title, awards_overview)
    @title = title
    @awards_overview = if awards_overview == 'N/A'
                         OVERVIEW_NO_AWARDS
                       else
                         awards_overview
                       end
  end
end
