class MovieNotFound < StandardError
  def initialize(msg = 'Movie not found')
    super
  end
end
