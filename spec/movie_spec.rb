require 'spec_helper'
require 'web_mock'

describe 'movie' do
  it 'returns awards overview' do
    movie = Movie.new('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
    expect(movie.awards_overview).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
  end

  it 'returns no known awards' do
    movie = Movie.new('Titanic', 'N/A')
    expect(movie.awards_overview).to eq('No known awards')
  end
end
